#        PongTV
# Pong Game on a PAL TV
#
# author :thomas Hocedez / Blenderlab.fr
# 11 Fevrier 2017
# v 1.0 : E.S.M.E Workshop Release
#
 

#include <TVout.h>
#include "fontALL.h"

TVout TV;
unsigned char balle_x, balle_y;
unsigned char dir_x, dir_y;
int paddle_A, paddle_B;
int score_A, score_B;

void setup()  {
  TV.begin(PAL, 120, 56); //for devices with only 1k sram(m168) use TV.begin(_NTSC,128,56)
  balle_x = 4;
  balle_y = 28;
  paddle_A = TV.vres() / 2 - 3 ;
  paddle_B = TV.vres() / 2 - 3 ;
  dir_x = 1;
  dir_y = 1;
  pinMode(A0, INPUT);
  score_A = 0;
  score_B = 0;
}

void dessiner_paddles() {
  for (int i = 0; i < 6; i++) {
    TV.set_pixel(2, paddle_A + i, 1);
    TV.set_pixel(TV.hres() - 2, paddle_B + i, 1);

  }

}
void dessiner_balle() {

  TV.set_pixel(balle_x, balle_y, 1);
  TV.set_pixel(balle_x + 1, balle_y, 1);
  TV.set_pixel(balle_x, balle_y + 1, 1);
  TV.set_pixel(balle_x + 1, balle_y + 1, 1);


}

void bouger_balle() {

  if (balle_y == 0) {
    dir_y = 1;
  }
  if (balle_y == TV.vres() - 2) {
    dir_y = -1;
  }

  if (balle_x == TV.hres() - 4  && balle_y >= paddle_B - 1 && balle_y < paddle_B + 6) {
    dir_x = -1;
  }
  if (balle_x == 3 && balle_y >= paddle_A - 1 && balle_y < paddle_A + 6) {
    dir_x = 1;
  }
  if (balle_x == 0 ) {
    score_B++;
    reset();
  }
  
  if (    balle_x == TV.hres()) {
    score_A++;
    reset();
  }

  balle_x += dir_x;
  balle_y += dir_y;


}

void reset() {
  balle_x = TV.hres() / 2;
  balle_y = TV.vres() / 2;
  delay(50000);
  int win=0;
  if (score_A==10 ){
    win=1;
  }
  if (score_B==10){
    win=2;
    
  }
  if (win>0){
    TV.clear_screen();
    TV.print(2,10,"Winner:");
    if (win==1 ) TV.print(2,32,"A");
    if (win==2 ) TV.print(2,32,"B");
        delay(500000);
    score_A=score_B=0;
  }

  int DE = random(10);
  
  if (DE > 5) {
    dir_x = -1;
  } else {
    dir_x = 1;
  }
  DE = random(10);
  if (DE > 5) {
    dir_y = -1;
  } else {
    dir_y = 1;
  }
}

void bouger_paddle() {
  int varA = analogRead(A0);
  int varB = analogRead(A1);

  paddle_A = map(varA, 0 , 1024, 0, TV.vres() - 6);
  paddle_B = map(varB, 0 , 1024, 0, TV.vres() - 6);
}
void loop() {
  TV.clear_screen();
  TV.select_font(font8x8);
  TV.print(2, 2, score_A);
  TV.print(100, 2, score_B);

  int milieu = TV.hres() / 2;
  for (int x = 0; x < TV.vres(); x += 2) {
    TV.set_pixel(milieu, x, 1);
  }
  bouger_balle();
  bouger_paddle();
  dessiner_balle();
  dessiner_paddles();


  TV.delay(10);
}
