# PongTV

Example on how to use an Arduino on a TV.

PongTV is an Arduino sketch & electronic tutorial to build a PongGame on a TV.

It's really easy, so you can teach/learn basic electronics, video signal, coding.

* **Electronics** - A bit of soldering, how potentiometers works, use of an Arduino
* **Video Signal** - Usage of Signal & Sync messages, how Arduino emulates them
* **Coding** - Code a gamefield, a ball, make it move, make it bounce, add paddles, make them move, limit bounce to paddles, add scoring ! Tada !

## Electronics

Needs : 1k Ohm resistor, 470 Ohm resistor, 2x 10k potetiometers, 1 AV connector.

* Connect 1k resistor on pin 9 of arduino (would be the sync generator)
* Connect 470 resistor on pin 7 of arduino (would be the data generator)
* Connect the left pin of first potentiometer to 5V
* Connect the right pin of first potentiometer to GND
* Connect the center pin of first potentiometer to A0 (would be the left player)
* Connect the left pin of second potentiometer to 5V
* Connect the right pin of second potentiometer to GND
* Connect the center pin of second potentiometer to A1 (would be the right player)

* Solder the 2 resistors together 
* Connect them to TV/AV cable (center wire)
* Connect the TV/AV cable to ground (external shield)


## Code
* Add the  [TVOut](https://code.google.com/archive/p/arduino-tvout/downloads)  library
* Upload the sketch on your arduino


## Play
* It's a pong, not hard to play.

## More 

Feel free to hack everything. you can add colors, sound, difficulties & so on. This project is a base Workshop.
Enjoy.

